import React from 'react';
import { Container } from 'rebass';
import HeaderRow from './HeaderRow';
import TableRow from './TableRow';

const Table = props => (
  <div>
    <Container>
      <HeaderRow />
      {props.data.map(product => <TableRow product={product} />)}
    </Container>
  </div>
);

export default Table;
