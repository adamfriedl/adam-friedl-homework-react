import React from 'react';
import { Sticky, Heading } from 'rebass';

const NavBar = () => (
  <Sticky p={1} my={2} top left color="black" bg="white">
    <Heading>Rakuten Products</Heading>
  </Sticky>
);

export default NavBar;
