import React from 'react';
import { Row, Flex, Truncate } from 'rebass';

const HeaderRow = () => (
  <Row bg={'blue'} py={[2, 3]} style={{ borderBottom: '1px grey solid' }}>
    <Flex wrap w={1 / 3} px={[1, 2]}>
      <Truncate>Product Name</Truncate>
    </Flex>
    <Flex wrap w={1 / 3} py={[1, 2]}>
      <Truncate>SKU</Truncate>
    </Flex>
    <Flex wrap w={1 / 3}>
      <Truncate>Advertiser</Truncate>
    </Flex>
  </Row>
);

export default HeaderRow;
