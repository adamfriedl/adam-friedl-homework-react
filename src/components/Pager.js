import React from 'react';
import { Box, Button } from 'rebass';

const Pager = props => (
  <Box p={2}>
    <Button mr={[2, 3]} onClick={() => props.onPrevClick()}>
      Prev
    </Button>
    <Button ml={[2, 3]} onClick={() => props.onNextClick()}>
      Next
    </Button>
  </Box>
);

export default Pager;
