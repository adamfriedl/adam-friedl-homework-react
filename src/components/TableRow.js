import React from 'react';
import { Row, Flex, Truncate } from 'rebass';

const TableRow = props => (
  <Row py={[1, 2]} style={{ borderBottom: '1px grey solid' }}>
    <Flex wrap w={1 / 3} px={[1, 2]}>
      <Truncate>{props.product.product_name}</Truncate>
    </Flex>
    <Flex wrap w={1 / 3} px={[1, 2]}>
      <Truncate>{props.product.product_sku}</Truncate>
    </Flex>
    <Flex wrap w={1 / 3} px={[1, 2]}>
      <Truncate>{props.product.advertiser.advertiser_name}</Truncate>
    </Flex>
  </Row>
);

export default TableRow;
