import React, { Component } from 'react';
import { Provider } from 'rebass';
import axios from 'axios';
import NavBar from './components/NavBar';
import Table from './components/Table';
import Pager from './components/Pager';

const parse = require('parse-link-header');

class App extends Component {
  state = {
    productData: [],
    prevLink: null,
    nextLink: null,
  };

  componentDidMount() {
    const url = 'http://localhost:3001/products';
    axios.get(url).then(res => {
      const links = parse(res.headers.link);
      console.log(links);
      const array = res.data;
      this.setState({
        productData: array,
        nextLink: links.next.url,
      });
    });
  }

  nextPage = () => {
    const url = this.state.nextLink;
    axios.get(url).then(res => {
      const links = parse(res.headers.link);
      console.log(links);
      const array = res.data;
      this.setState({
        productData: array,
        nextLink: links.next ? links.next.url : null,
        prevLink: links.prev ? links.prev.url : null,
      });
    });
  };

  prevPage = () => {
    const url = this.state.prevLink;
    axios.get(url).then(res => {
      const links = parse(res.headers.link);
      console.log(links);
      const array = res.data;
      this.setState({
        productData: array,
        nextLink: links.next ? links.next.url : null,
        prevLink: links.prev ? links.prev.url : null,
      });
    });
  };

  render() {
    return (
      <Provider
        theme={{
          fonts: {
            sans: '"Avenir Next", Helvetica, sans-serif',
          },
        }}
      >
        <div>
          <NavBar />
          <Pager
            prevLink={this.state.prevLink}
            nextLink={this.state.nextLink}
            onNextClick={() => this.nextPage()}
            onPrevClick={() => this.prevPage()}
          />
          <Table data={this.state.productData} />
          {this.state.linkHeader}
        </div>
      </Provider>
    );
  }
}

export default App;
