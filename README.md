# Frontend Solution

## Overview

This React frontend requests data from the [backend detailed here](https://github.com/adamfriedl/products-table-api) and presents it in a table.

## Installation

Download or clone the repository, `cd` into its directory on your machine, and run `yarn`. This will install all dependencies, which are, for the most part, standard create-react-app packages, plus a few extras for help with things like parsing headers.

To use, start the server with `yarn start`, which will automatically open a tab in your default browser to the default port (which is localhost:3000). Be sure to have the backend rails server running as well to respond to this frontend's request. See its [repo](https://github.com/adamfriedl/products-table-api) for detailed instructions.

## Visual design

I used a component library called [Rebass](http://jxnblk.com/rebass/). It does not contain a prefab table component, so I made one myself that is composed of other components (e.g., rows, flexboxes, etc.)

## How it works

Upon mounting, the app makes a GET request to the api endpoint and receives a JSON response, which it stores in state. It passes this data down to components within the table to display. To avoid slowdowns, responses are paginated (currently set at 30 items). The response header includes 'next' and 'previous' links, which I grab from the headers and put into state. These are passed down the the pagination buttons.

## Tests

I haven't provided tests, which is of course suboptimal. I'm having an issue getting any jest tests to run at the moment on my machine, but haven't succeeded in debuggin yet. (I will, though!)

## Potential improvements

Tests, obviously, as well as sharpening up the visual design. And I would likely try to DRY up the functions handling pagination.
